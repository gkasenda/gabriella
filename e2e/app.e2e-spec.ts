import { gabriellaPage } from './app.po';

describe('gabriella App', () => {
  let page: gabriellaPage;

  beforeEach(() => {
    page = new gabriellaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
